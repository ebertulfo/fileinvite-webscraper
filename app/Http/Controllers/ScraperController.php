<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Goutte;
use League\Csv\Writer;

class ScraperController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function scrape() {

        $crawler = Goutte::request("GET", "https://www.icas.com/find-a-ca?results=ca&meta_G=Member&start_rank=22131");
        $this->accountants = array();
        $this->accountantInfo = array();
        $this->header = array();

        do{
            $crawler->filter('article > h3 > a')->each( function($item) {
                $accountant = Goutte::click($item->link());
                $this->accountantInfo["name"]= $accountant->filter("#profile > div > h1")->text(); 
                $accountant->filter("dl")->each( function($details) {
                    if( $details->filter("dt")->count() > 0 && $details->filter("dd")->count() > 0) {
                        $this->accountantInfo[snake_case(str_replace(":", "", $details->filter("dt")->text()))] = $details->filter("dd")->text();
                    }
                });
                $this->accountants[] = $this->accountantInfo;
                $this->header = array_unique(array_merge(array_keys($this->accountantInfo), $this->header));
                $this->accountantInfo = array();
            });

            try {
                $link = $crawler->selectLink(">")->link();
                $crawler = Goutte::click($link);
            } catch(\Exception $e) {
                break;
            } 
           
        } while( $crawler != null );
    
        $csvInput = $this->formatData($this->header, $this->accountants);

        //Used this method instead of creating the file and returning it. Because for some weird reason, using the create + return file method appends some unexplained numbers at the end of the CSV file.
        echo "Download finish!";
        return response($this->createCsv($this->header, $csvInput))
            ->header('Content-Encoding', 'none')
            ->header('Content-Type', 'text/csv; charset=UTF-8')
            ->header('Content-Disposition', 'attachment; filename="accountants.csv"')
            ->header('Content-Description', 'File Transfer');

    }

    private function formatData($headers, $data) {

        $result = array();
        foreach( $data as $datum ) {
            foreach( $headers as $header ) {
                $entry[$header] = isset($datum[$header]) ? $datum[$header] : "";
            }
            array_push($result, $entry);
        }
        
        return $result;

    }

    private function createCsv($header, $entries, $fileName = "accountants") {

        $csv = Writer::createFromFileObject(new \SplTempFileObject());
        $csv->insertOne($header);
        $csv->insertAll($entries);

        return $csv->getContent();

    }
}
